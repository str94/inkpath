<?php

namespace App\Http\Controllers;

use App\Models\Guardian;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function data(Request $request)
    {
        return response()->json(Guardian::request($request->toArray()));
    }
}
