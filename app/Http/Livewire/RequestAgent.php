<?php

namespace App\Http\Livewire;

use App\Models\CsvWriter;
use App\Models\Guardian;
use Illuminate\Support\Facades\Http;
use Livewire\Component;

class RequestAgent extends Component
{
    public $tags = [];
    public $sections = [];

    public $parameters = [
        "q" => null,
        "tag" => [],
        "section" => [],
        "from-date" => null,
        "to-date" => null,
        "page-size" => 10,
    ];

    public $query;
    public $response;

    // rule must be set for each parameter even if empty
    protected $rules = [
        "parameters.q" => "",
        "parameters.tag.*" => "",
        "parameters.section.*" => "",
        "parameters.from-date" => "",
        "parameters.to-date" => "",
        "parameters.page-size" => "numeric|min:1|max:50",
    ];

    // set nicer names for the parameters to help users
    protected $validationAttributes = [
        "parameters.q" => "Search",
        "parameters.tag.*" => "Tags",
        "parameters.section.*" => "Sections",
        "parameters.from-date" => "From Date",
        "parameters.to-date" => "To Date",
        "parameters.page-size" => "No. Results",
    ];

    // fired when the component is first mounted
    public function mount()
    {
        // attempt to load all selectable sections
        try {
            $this->sections = Guardian::loadElement("sections");
        } catch (\Exception $e) {
            $this->addError("parameters.sections.*", "Failed to load sections: " . $e->getMessage());
        }
    }

    // fires every time anything is updated
    public function render()
    {
        return view('livewire.request-agent');
    }

    // function which will send the request to the API
    public function send()
    {
        $this->validate();

        // some extra validation
        if (count($this->parameters['section']) < 1) {
            return $this->addError("parameters.section.*", "At least 1 section must be selected");
        }

        // prep work so we can show a query string to the user
        $qs = Guardian::cleanseParameters($this->parameters);

        // dont show the user the api key
        if(array_key_exists("api-key", $qs)){
            unset($qs["api-key"]);
        }

        // build a query string
        $qs = http_build_query($qs);

        // check if the query string is empty, if not, show the user
        $this->query = !empty($qs) ? config("app.url") . "/api/data?$qs" : null;

        // try/catch block used here as external API calls are unpredictable
        try {
            // fetch the results from the API
            $this->response = Guardian::request($this->parameters);
        } catch (\Exception $e) {
            // If something went wrong, let the user know
            $this->response = $e->getMessage();
        }
    }

    // function to convert the output to a CSV and download
    public function csv()
    {
        try {
            // make sure our response is still an array and has a results key
            if(is_array($this->response) && array_key_exists('results', $this->response)){
                // write the content to a CSV file
                return CsvWriter::create($this->response['results'], 'guardian_news.csv');
            }
        } catch (\Exception $e) {
            $this->response = $e->getMessage();
        }
    }

    // function will run every time something is updated
    public function updated($name, $value)
    {
        if (strpos($name, "parameters.section") !== false) {
            // clear any previous errors on the tag field
            $this->resetErrorBag(["parameters.section.*", "parameters.tag.*"]);

            // the sections array has been updated, load relevant tags
            try {
                $this->tags = Guardian::loadElement("tags", ["section" => $this->parameters["section"]]);

                // clean up our tags parameter to only include tags for current subjects
                foreach ($this->parameters['tag'] as $key => $tag) {
                    if (!collect($this->tags)->pluck("id")->contains($tag)) {
                        // tag isn't in our new list, lets remove it for now
                        unset($this->parameters['tag'][$key]);
                    }
                }
            } catch (\Exception $e) {
                $this->addError("parameters.tag.*", "Failed to load relevant tags: " . $e->getMessage());
            }
        }
    }

    public function toggleSections()
    {
        // check if the section parameter is currently empty and either fill or empty it
        if(count($this->parameters['section']) < 1){
            $this->parameters["section"] = collect($this->sections)->pluck("id")->toArray();
        } else {
            $this->parameters["section"] = [];
        }
    }

    public function toggleTags()
    {
        // check if the tag parameter is currently empty and either fill or empty it
        if(count($this->parameters['tag']) < 1){
            $this->parameters["tag"] = collect($this->tags)->pluck("id")->toArray();
        } else {
            $this->parameters["tag"] = [];
        }
    }
}
