<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Response;

class CsvWriter extends Model
{
    use HasFactory;

    public static function create($content, $filename)
    {

        $callback = function() use ($content) {
            // create an output stream for us to write to
            $file = fopen('php://output', 'w');

            // quick check to make sure our file was created
            if(!$file){
                throw new \Exception("Failed to create temporary file for export");
            }

            // make sure there is at least 1 item for us to get the column names from
            if (count($content)) {
                // print the array keys to the csv
                fputcsv($file, array_keys($content[array_key_first($content)]));
            }

            // print the results to the csv
            foreach($content as $c){
                fputcsv($file, $c);
            }

            // remove the file from memory
            fclose($file);
        };

        return response()->streamDownload($callback, $filename);
    }
}
