<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class Guardian extends Model
{
    use HasFactory;

    const URL = "https://content.guardianapis.com";
    const KEY_LOCATION = "services.guardian.key";

    public static function loadElement($element, $parameters = [])
    {
        // clean up our input into something that will work
        $parameters = self::cleanseParameters($parameters);

        // perform the api call and return tags as an array
        $response = Http::get(self::URL."/$element", $parameters)->json();

        // let's just make sure we have the expected response format
        if(!array_key_exists("response", $response) || !array_key_exists("results", $response["response"])){
            throw new \Exception("unexpected response format");
        }

        return $response["response"]["results"];
    }

    public static function request($parameters)
    {
        // clean up our input into something that will work
        $parameters = self::cleanseParameters($parameters);

        // perform the api call and return results in json format
        $response = Http::get(self::URL."/search", $parameters)->json();

        // let's just make sure we have the expected response, if not, return what we've got
        return array_key_exists("response", $response)
            ? $response['response']
            : $response;
    }

    public static function cleanseParameters($parameters)
    {
        // check for any arrays and convert to OR separated strings
        foreach ($parameters as $key => $value) {
            if (is_array($value)) {
                $parameters[$key] = trim(implode('|', $value), '|');
            }
        }

        // add our API key to the parameters to pass authentication
        $parameters["api-key"] = config(self::KEY_LOCATION);

        // clear any unused parameters
        return array_filter($parameters);
    }
}
