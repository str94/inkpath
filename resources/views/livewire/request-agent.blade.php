<div class="grid grid-cols-1 lg:grid-cols-2 gap-6">
    <div class="space-y-4">
        <h2 class="text-lg font-semibold uppercase">Parameters</h2>

{{--        <div class="space-y-1">--}}
{{--            <x-jet-label for="api_key">API Key</x-jet-label>--}}
{{--            <x-jet-input type="text" class="w-full" id="api_key" wire:model="api_key" placeholder="API Key" required/>--}}
{{--            <p class="text-sm italic text-gray-600">--}}
{{--                To create an API key, go to <a class="text-blue-400 hover:text-blue-600 transition" href="/user/api-tokens">API Tokens</a>.--}}
{{--            </p>--}}
{{--            <x-jet-input-error for="api_key" />--}}
{{--        </div>--}}

        <div class="space-y-1">
            <x-jet-label for="search">Search</x-jet-label>
            <x-jet-input type="text"
                         class="w-full"
                         id="search"
                         wire:model="parameters.q"
                         placeholder="Search for Specific Keywords" />
            <x-jet-input-error for="parameters.q" />
        </div>

        <div class="space-y-1">
            <div class="flex justify-between items-center">
                <x-jet-label for="sections">Sections (Match Any)<span class="text-red-600">*</span></x-jet-label>
                <a class="block text-blue-400 hover:text-blue-600 transition"
                   wire:click.prevent="toggleSections"
                   href="#">
                    @if(count($parameters['section']) < 1)
                        Select All
                    @else
                        Deselect All
                    @endif
                </a>
            </div>
            <div class="w-full grid grid-cols-2 lg:grid-cols-4 gap-2 p-2 border border-gray-300 rounded-md shadow max-h-32 overflow-auto"
                 id="sections">
                @foreach($sections as $index => $section)
                    <x-jet-label>
                        <x-jet-checkbox wire:model="parameters.section.{{ $index }}" value="{{ $section['id'] }}" />
                        {{ $section['webTitle'] }}
                    </x-jet-label>
                @endforeach
            </div>
            <x-jet-input-error for="parameters.section.*" />
        </div>

        @if(count($tags))
            <div class="space-y-1">
                <div class="flex justify-between items-center">
                    <x-jet-label for="tags">Tags (Match Any)</x-jet-label>
                    <a class="block text-blue-400 hover:text-blue-600 transition"
                       wire:click.prevent="toggleTags"
                       href="#">
                        @if(count($parameters['tag']) < 1)
                            Select All
                        @else
                            Deselect All
                        @endif
                    </a>
                </div>
                <div class="w-full grid grid-cols-2 lg:grid-cols-4 gap-2 p-2 border border-gray-300 rounded-md shadow max-h-32 overflow-auto"
                     id="tags">
                    @foreach($tags as $index => $tag)
                        <x-jet-label>
                            <x-jet-checkbox wire:model="parameters.tag.{{ $index }}" value="{{ $tag['id'] }}" />
                            {{ $tag['webTitle'] }}
                        </x-jet-label>
                    @endforeach
                </div>
                <x-jet-input-error for="parameters.tag.*" />
            </div>
        @endif

        <div class="space-y-1">
            <x-jet-label for="from_date">From Date</x-jet-label>
            <x-jet-input class="w-full" type="date" id="from_date" wire:model="parameters.from-date" :max="date('Y-m-d')" />
            <x-jet-input-error for="parameters.from-date" />
        </div>

        <div class="space-y-1">
            <x-jet-label for="to_date">To Date</x-jet-label>
            <x-jet-input class="w-full" type="date" id="to_date" wire:model="parameters.to-date" :max="date('Y-m-d')" />
            <x-jet-input-error for="parameters.to-date" />
        </div>

        <div class="space-y-1">
            <x-jet-label for="page_length">No. Results</x-jet-label>
            <x-jet-input class="w-full" type="number" id="page_length" wire:model="parameters.page-size" min="1" max="50" />
            <x-jet-input-error for="parameters.page-size" />
        </div>

        <x-jet-button wire:click.prevent="send">Send</x-jet-button>
    </div>

    <div class="space-y-2">
        <div class="flex justify-between items-center">
            <h2 class="text-lg font-semibold uppercase">Response</h2>

            @if(is_array($this->response) && array_key_exists('results', $this->response))
                <x-jet-button wire:click.prevent="csv">Export CSV</x-jet-button>
            @else
                <x-jet-button disabled wire:click.prevent="csv">Export CSV</x-jet-button>
            @endif
        </div>

        <div class="w-full p-2 border border-gray-300 rounded-md max-h-96 overflow-auto" wire:loading.delay.class="bg-gray-200">
            <p>{{ !empty($response) ? json_encode($response) : "No Response" }}</p>
        </div>

        @if(!empty($query))
        <div>
            <x-jet-label for="query_ran">Last Query</x-jet-label>
            <div class="w-full p-2 border border-gray-300 rounded-md overflow-auto" wire:loading.delay.class="bg-gray-200">
                <p>{{ $query }}</p>
            </div>
        </div>
        @endif
    </div>
</div>
