<x-app-layout>
    <x-slot name="header">
        <h1 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h1>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="p-6 bg-white overflow-hidden shadow-xl sm:rounded-lg space-y-6">
                <p>
                    Interact with The Guardian API below or use your favourite API Client with our endpoint
                    <a class="text-blue-400 hover:text-blue-600 transition"
                       href="/api/data">
                        {{ config("app.url") }}/api/data
                    </a>
                    but don't forget to pass your
                    <a class="text-blue-400 hover:text-blue-600 transition"
                       href="/user/api-tokens">
                        API key
                    </a>
                    as a Bearer token in the Authorization header.
                </p>

                @livewire("request-agent")
            </div>
        </div>
    </div>
</x-app-layout>
