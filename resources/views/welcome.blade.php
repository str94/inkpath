<x-guest-layout>
    <div class="py-12">
        <div class="max-w-md mx-auto sm:px-6 lg:px-8">
            <div class="p-12 space-y-12 bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="flex flex-col items-center gap-6">
                    <x-jet-authentication-card-logo />
                    <h1 class="font-bold uppercase">Welcome</h1>
                </div>

                <div class="space-y-6">
                    <a class="block" href="{{ route("login") }}">
                        <x-jet-secondary-button class="w-full justify-center">Login</x-jet-secondary-button>
                    </a>

                    <a class="block" href="{{ route("register") }}">
                        <x-jet-secondary-button class="w-full justify-center">Register</x-jet-secondary-button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</x-guest-layout>
