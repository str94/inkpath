<h1>ReadMe</h1>

<h2>Using the interfance</h2>

<ol>
    <li>Log in/Register for an account to access the interface</li>
    <li>Go to the dashboard in the navigation menu</li>
    <li>Create some filters to interact with the API</li>
    <Li>Click 'Send' to load your results, which are displayed in JSON</Li>
    <li>Click 'Export CSV' to export the results to a CSV file</li>
</ol>

<h2>Using the API</h2>

<ol>
    <li>Log in/Register for an account</li>    
    <li>Go to 'API Tokens' under your name in the navigation menu</li>
    <li>Create an API token by giving it a name, this token will be valid for 5 minutes</li>
    <li>Pass the token in the Authorization Header as a Bearer token</li>
    <li>Send some parameters along with your request to filter the results (uri's are provided via the interface when sending queries)</li>
</ol>
